package Assignment3d8;

import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    static String inputIP, inputPort;

    public static void main(String[] args) {
        final Socket clientSocket;
        final BufferedReader in;
        final PrintWriter out;
        final Scanner input = new Scanner(System.in);

        try{
            try (InputStream inp = new FileInputStream("/Users/ada-nb187/Documents/d07/src/Assignment3d8/config.properties")) {

                Properties prop = new Properties();

                prop.load(inp);

                inputIP = prop.getProperty("ip");
                inputPort = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            clientSocket = new Socket(inputIP, Integer.parseInt(inputPort));

            out = new PrintWriter(clientSocket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            Thread sender = new Thread(new Runnable() {
                String msg;
                @Override
                public void run() {
                    while (true) {
                        msg = input.nextLine();
                        out.println(msg);
                        out.flush();
                    }
                }
            });
            sender.start();

            Thread receive = new Thread(new Runnable() {
                String msg;
                @Override
                public void run() {
                    try {
                        msg = in.readLine();

                        while(msg != null) {
                            if (msg.equals("exit")) {
                                break;
                            }
                            System.out.println("Server: "+msg);
                            msg = in.readLine();
                        }

                        System.out.println("Keluar dari obrolan");
                        out.close();
                        clientSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            receive.start();
        }catch(Exception e){System.out.println(e);}
    }
}