package Assignment2d8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MultiT1 extends Thread{

    private ArrayList<Nilai> nilai = new ArrayList();

    public MultiT1 (ArrayList <Nilai> nilai) {
        this.nilai = nilai;
    }

    public void run() {
        Collections.sort(nilai, new Nilai.SortById());

        System.out.println("\nSorted by ID");

        Iterator<Nilai> itr = nilai.iterator();
        int i = 0;

        System.out.printf("%-10s %-10s %-10s %-10s %-10s %n", "ID", "Nama", "Bhs Inggris", "Fisika", "Algoritma");

        while (itr.hasNext()) {
            Nilai daftarMhs = itr.next();
            System.out.printf("%-10s %-10s %-10s %-10s %-10s %n", daftarMhs.getId(), daftarMhs.getNama(), daftarMhs.getNilaiBI(), daftarMhs.getNilaiFis(), daftarMhs.getNilaiAlgo());
        }

    }




}
