package Assignment2d8;

public class Nilai extends Mahasiswa {
    double nilaiBI;
    double nilaiFis;
    double nilaiAlgo;

    public Nilai(int id, String nama, double nilaiBI, double nilaiFis, double nilaiAlgo) {
        super(id, nama);
        this.nilaiBI = nilaiBI;
        this.nilaiFis = nilaiFis;
        this.nilaiAlgo = nilaiAlgo;
    }

    public double getNilaiBI() {
        return nilaiBI;
    }

    public double getNilaiFis() {
        return nilaiFis;
    }

    public double getNilaiAlgo() {
        return nilaiAlgo;
    }

}

