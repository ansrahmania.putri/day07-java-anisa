package Assignment2d8;

import Assignment1d8.Multi1;
import Assignment1d8.Multi2;

import java.util.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main extends Thread{

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;

        String regexEmail ="^(.+)@(.+)$";

        String regexPassword = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";

        Properties prop = new Properties();


        try (InputStream read = new FileInputStream("/Users/ada-nb187/Documents/d07/src/Assignment2d8/credential.txt")) {

            // load a properties file
            prop.load(read);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //email
        System.out.println("Masukkan email: ");
        String email = input.nextLine();
        System.out.println(email.matches(prop.getProperty("username")));


        //password
        System.out.println("Masukkan password: ");
        String pass = input.nextLine();
        System.out.println(pass.matches(prop.getProperty("password")));

        ArrayList<Nilai> nilai = new ArrayList<>();

        do {
            System.out.println("Menu");
            System.out.println("1.  Create&Input Data Mahasiswa");
            System.out.println("2.  Tampilkan Laporan Data Mahasiswa");
            System.out.println("3.  Tampilkan pada Layar & Tulis ke file");
            System.out.println("4.  EXIT");
            System.out.println("Pilih Menu (1/2/3/4)");
            pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("ID: ");
                    int id = input.nextInt();
                    System.out.print("Nama: ");
                    String nama = input.next();
                    System.out.print("Nilai B.Inggris: ");
                    double nilaiBI = input.nextDouble();
                    System.out.print("Nilai Fisika: ");
                    double nilaiFis = input.nextDouble();
                    System.out.print("Nilai Algoritma: ");
                    double nilaiAlgo = input.nextDouble();

                    Nilai mhs = new Nilai(id, nama, nilaiBI, nilaiFis, nilaiAlgo);
                    nilai.add(mhs);
                    break;

                case 2:
                    Collections.sort(nilai, new Nilai.SortById());

                    System.out.println("\nSorted by ID");

                    Iterator<Nilai> itr = nilai.iterator();
                    int i = 0;

                    System.out.printf("%-10s %-10s %-10s %-10s %-10s %n", "ID", "Nama", "Bhs Inggris", "Fisika", "Algoritma");

                    while (itr.hasNext()) {
                        Nilai daftarMhs = itr.next();
                        System.out.printf("%-10s %-10s %-10s %-10s %-10s %n", daftarMhs.getId(), daftarMhs.getNama(), daftarMhs.getNilaiBI(), daftarMhs.getNilaiFis(), daftarMhs.getNilaiAlgo());
                    }
                    break;
                case 3:
                    MultiT1 t1 = new MultiT1(nilai);
                    t1.start();
                    MultiT2 t2 = new MultiT2(nilai);
                    t2.start();
                    break;

                default:
                    if (pilihan==4) {
                        break;
                    } else {
                        System.out.println("Menu yang anda pilih tidak tersedia");
                    }
            }

        } while (pilihan != 4);


















    }


}
