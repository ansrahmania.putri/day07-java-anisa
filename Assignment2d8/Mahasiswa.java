package Assignment2d8;

import java.util.Comparator;

public class Mahasiswa {
    int id;
    String nama;

    public Mahasiswa(int id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public static class SortById implements Comparator<Mahasiswa> {

        public int compare(Mahasiswa a,Mahasiswa b) {
            return (int) (a.id - b.id);
        }
    }


}
