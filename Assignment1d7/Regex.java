package Assignment1d7;

import java.util.Scanner;

public class Regex {



    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String regexEmail ="^(.+)@(.+)$";

        String regexPassword = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,20}$";


        //email
        System.out.println("Masukkan email: ");
        String email = input.nextLine();
        System.out.println(email.matches(regexEmail));


        //password
        System.out.println("Masukkan password: ");
        String pass = input.nextLine();
        System.out.println(pass.matches(regexPassword));


    }




}
